/* Program 1:
Write a program to print the value and size of the below variables.Take all
the values from the user
num=10
chr = ‘S’
rs = 55.20
crMoney = 542154313480.544543 */

#include<stdio.h>
void main()
{
	int num=10;
	char ch='S';
	float rs=55.20;
	double crMoney=542154313480.544543;
	printf("Size of %d : %ld\n",num,sizeof(num));
	printf("Size of %c : %ld\n",ch,sizeof(ch));
	printf("Size of %f : %ld\n",rs,sizeof(rs));
	printf("Size of %f : %ld\n",crMoney,sizeof(crMoney));
}