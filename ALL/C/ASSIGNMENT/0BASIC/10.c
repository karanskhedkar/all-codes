/* Program 8: Write a program, take a character and print whether it is in
UPPERCASE or lowercase. Take all the values from the user */

#include<stdio.h>
void main()
{
	char chr;
	printf("Enter character : ");
	scanf("%c",&chr);
	if(('a'<=chr && chr<='z')||('A'<=chr && chr<='Z'))
	{
		if(chr=='a'||chr=='e'||chr=='i'||chr=='o'||chr=='u'||chr=='A'||chr=='E'||chr=='I'||chr=='O'||chr=='U')
			printf("%c is an vowel\n",chr);
		else
			printf("%c is an consonant\n",chr);
	}
	else
		printf("%c is not an alphabet, invalid input\n",chr);
}
