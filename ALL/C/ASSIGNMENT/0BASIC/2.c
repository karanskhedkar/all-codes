/* Program 2: WAP print the value of the below expressions.
x =9 ;
ans = ++x + x++ + ++x
Print ans value and print x
ans1= ++x + ++x + ++x + ++x
Print ans1 value and print x
ans2 = x++ + x++ + ++x + x++ + ++x
Print ans2 value and print x
ans3 = x++ + x++ + x++ + x++
Print ans3 value and print x */

#include<stdio.h>
void main()
{
	int x,ans,ans1,ans2,ans3;
	printf("Enter value of x : ");
	scanf("%d",&x);
	ans = ++x + x++ + ++x;
	printf("ans = %d\n",ans);
	printf("x = %d\n",x);
	ans1= ++x + ++x + ++x + ++x;
	printf("ans1 = %d\n",ans);
	printf("x = %d\n",x);
	ans2 = x++ + x++ + ++x + x++ + ++x;
	printf("ans2 = %d\n",ans);
	printf("x = %d\n",x);
	ans3 = x++ + x++ + x++ + x++;
	printf("ans3 = %d\n",ans);
	printf("x = %d\n",x);
}