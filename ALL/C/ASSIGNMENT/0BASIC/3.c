/* Program 3: WAP to find max among 2 numbers. Take all the values from the
user */

#include<stdio.h>
void main()
{
	int num1,num2;
	printf("Enter first number : ");
	scanf("%d",&num1);
	printf("Enter second number : ");
	scanf("%d",&num2);
	if(num1>num2)
		printf("%d>%d\nTherefore %d is greater\n",num1,num2,num1);
	else if(num2>num1)
		printf("%d>%d\nTherefore %d is greater\n",num2,num1,num2);
	else
		printf("%d=%d\nTherefore both number are equal\n",num1,num2);
}