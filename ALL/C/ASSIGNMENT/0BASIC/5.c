/* Program 5: WAP to take numerical input from the user and find whether the number is
divisible by 5 and 11. Take all the values from the user */

#include<stdio.h>
void main()
{
	int num;
	printf("Enter number : ");
	scanf("%d",&num);
	int temp1,temp2;
	temp1=num%5;
	temp2=num%11;
	if(temp1==0 && temp2==0)
		printf("%d is divisible by both 5 and 11\n",num);
	else if(temp1==0)
		printf("%d is divisible by only 5\n",num);
	else if(temp2==0)
		printf("%d is divisible by only 11\n",num);

}