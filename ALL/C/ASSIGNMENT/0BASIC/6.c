/* Program 6:
Write a program to check if a number is even or odd. Take all the values
from the user */

#include<stdio.h>
void main()
{
	int num;
	printf("Enter number : ");
	scanf("%d",&num);
	if(num%2==0)
		printf("%d is even\n",num);
	else
		printf("%d is odd\n",num);
}