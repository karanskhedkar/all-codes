/* Program 7:
Write a program, take a number and print whether it is less than 10 or greater
than 10. Take all the values from the user */

#include<stdio.h>
void main()
{
	int num;
	printf("Enter number : ");
	scanf("%d",&num);
	if(num>10)
		printf("%d is greater than 10\n",num);
	else if(num<10)
		printf("%d is smaller than 10\n",num);
	else
		printf("Equal\n");
}