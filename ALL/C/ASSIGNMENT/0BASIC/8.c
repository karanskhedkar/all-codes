
/* Program 8: Write a program, take a character and print whether it is in
UPPERCASE or lowercase. Take all the values from the user */

#include<stdio.h>
void main()
{
	char chr;
	printf("Enter character : ");
	scanf("%c",&chr);
	if('a'<=chr && chr<='z')
		printf("%c is an lowercase alphabet\n",chr);
	else if('A'<=chr && chr<='Z')
		printf("%c is an uppercase alphabet\n",chr);
	else
		printf("%c is not an alphabet, please give a valid input\n",chr);
}