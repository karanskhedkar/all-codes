/* 1. Write a program to print the first 10 capital Alphabets */

#include<stdio.h>
void main()
{
	char ch1;
	printf("Enter starting character : ");
	scanf("%c",&ch1);
	int j=ch1;
	if('A'<=ch1 && ch1<='Z')
	{
		int num;
		printf("Enter number of character you want to print from %c : ",ch1);
		scanf("%d",&num);
		if(num>-1)
		{
			for(int i=0;i<=num;i++)
			{
				if(j%90==0)
				{
					printf("%c\n",j);
					j=65;
				}
				if(j==65)
					printf("%c\n",j);
				else
					printf("%c\n",j);
				j+=1;
			}
		}
		else
			printf("Invalid input for number of alphabets to print\n");
	}
	else
		printf("%c is not an capital alphabet, program terminated\n",ch1);
}