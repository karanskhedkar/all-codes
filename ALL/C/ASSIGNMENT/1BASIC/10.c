/* 10. Write a program to print the product of the first 10 numbers */

#include<stdio.h>
void main()
{
	int start,end;
	int pro=1;
	printf("Enter starting point : ");
	scanf("%d",&start);
	printf("Enter ending point : ");
	scanf("%d",&end);
	if(end>start)
	{
		for (int i=start;i<=end;i++)
		{
			pro*=i;
		}
		printf("Product of numbers from %d to %d = %d\n",start,end,pro);
	}
	else
		printf("Starting point should be lesser than ending point\n");

}