/* 2. Write a program to print the first 100 numbers */

#include<stdio.h>
void main()
{
	int start,num;
	printf("Enter starting point : ");
	scanf("%d",&start);
	printf("How many numbers do you want to print from %d : ",start);
	scanf("%d",&num);
	if(num>-1)
	{
		printf("%d numbers from %d are as below:\n",num,start);
		for (int i=start;i<(start+num);i++)
		{
			printf("%d\n",i);
		}
	}
	else
		printf("Invalid input for numbers to print after %d\n",start);
}