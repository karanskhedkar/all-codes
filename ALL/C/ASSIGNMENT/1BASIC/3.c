/* 3. Write a program to print the first ten, 3 digit number */

#include<stdio.h>
void main()
{
	int start,num,count;
	count=0;
	printf("Enter starting 3-digit number : ");
	scanf("%d",&start);
	int temp;
	if(start<0)
		temp=start*-1;
	while(temp>0)
	{
		count+=1;
		temp=temp/10;
	}
	if(count==3)
	{
		printf("How many numbers do you want to print from %d : ",start);
		scanf("%d",&num);
		if(num>-1)
		{
			printf("%d numbers from %d are as below:\n",num,start);
			for (int i=start;i<(start+num);i++)
			{
				printf("%d\n",i);
			}
		}
		else
			printf("Invalid input for numbers to print after %d\n",start);
	}
	else
		printf("%d is not a 3-digit number, program terminated\n",start);
}