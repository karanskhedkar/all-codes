/* 4. Write a program to print even numbers 1-100 */

#include<stdio.h>
void main()
{
	int start,end;
	printf("Enter starting point : ");
	scanf("%d",&start);
	printf("Enter ending point : ");
	scanf("%d",&end);
	if(end>start)
	{
		printf("Even numbers from %d to %d are as below:\n",start,end);
		for (int i=start;i<=end;i++)
		{
			if(i%2==0)
				printf("%d\n",i);
		}
	}
	else
		printf("Starting point should be lesser than ending point\n");

}