/* 6. Write a program to print reverse from 100-1 */

#include<stdio.h>
void main()
{
	int start,end;
	printf("Enter starting number : ");
	scanf("%d",&start);
	printf("Enter ending number : ");
	scanf("%d",&end);
	if(start<end)
	{
		printf("Numbers from %d to %d in reverse order are as below:\n",start,end);
		for(int i=end;i>=start;i--)
		{
			printf("%d\n",i);
		}
	}
	else
		printf("Starting point should be less than ending point, program terminated\n");
}