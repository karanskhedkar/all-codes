/* 7. Write a program to print a table of 12 */

#include<stdio.h>
void main()
{
	int num;
	printf("Enter number whose tabel is required : ");
	scanf("%d",&num);
	printf("Tabel of %d:\n",num);
	for(int i=1;i<11;i++)
	{
		printf("%d * %d = %d\n",num,i,num*i);
	}
}