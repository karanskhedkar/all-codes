/* 8. Write a program to print a table of 11 in reverse order */

#include<stdio.h>
void main()
{
	int num;
	printf("Enter number whose tabel is required : ");
	scanf("%d",&num);
	printf("Tabel of %d in reverse order:\n",num);
	for(int i=10;i>0;i--)
	{
		printf("%d * %d = %d\n",num,i,num*i);
	}
}