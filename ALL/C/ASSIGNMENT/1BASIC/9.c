/* 9. Write a program to print the sum of the first 10 odd numbers*/
#include<stdio.h>
void main()
{
	int start,end;
	int add=0;
	printf("Enter starting point : ");
	scanf("%d",&start);
	printf("Enter ending point : ");
	scanf("%d",&end);
	if(end>start)
	{
		for (int i=start;i<=end;i++)
		{
			add+=i;
		}
		printf("Sum of numbers from %d to %d = %d\n",start,end,add);
	}
	else
		printf("Starting point should be lesser than ending point\n");

}