/*Perfect Array
- Given an array of size N and you have to tell whether the array is perfect or not.
- An array is said to be perfect if it's reverse array matches the original array.
- If the array is perfect then print "PERFECT" else print "NOT PERFECT".*/

#include<stdio.h>
void main()
{
	int size=0;;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		int flag=0;
		printf("Enter Elements of array\n");
		for(int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		for(int j=0;j<size/2;j++)
		{
			if(arr[j]==arr[size-j-1])
			{
				continue;
			}
			else
			{
				flag=1;
				printf("NOT PERFECT ARRAY\n");
				break;
			}
		}
		if(flag==0)
		{
			printf("PERFECT ARRAY\n");
		}
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
