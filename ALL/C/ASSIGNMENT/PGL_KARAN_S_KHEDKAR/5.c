/*5. Largest Element :
- Find the third largest element in array.*/

#include<stdio.h>
void main()
{
	int size;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>2)
	{
		int arr[size];
		int lrgst;
		printf("Enter elements of array : \n");
		for (int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);

		}
		int temp=0;
		for (int i=0;i<size;i++)
		{
			for (int j=i+1;j<size;j++)
			{
				if(arr[i]>arr[j])
				{
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		lrgst=arr[size-1];
		int c=0;
		for(int j=size-1;-1<j;j--)
		{
			if(arr[j]<lrgst)
			{
				lrgst=arr[j];
				c+=1;
			}
			if(c==2)
				break;
		}
		
		printf("Third largest element : %d\n",lrgst);
	}
	else
		printf("Invalid input for array size. Minimum size should be 3 for finding third largest element\n");
}
