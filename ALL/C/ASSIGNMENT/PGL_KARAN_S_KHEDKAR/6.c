/*6. Number of occurrence
- Find the number of occurrences of X in array.*/

#include<stdio.h>
void main()
{
	int size=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		int fnum=0;
		int count=0;
		printf("Enter elements of array : \n");
		for(int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		printf("Enter number whose frequency is required : ");
		scanf("%d",&fnum);
		for(int i=0;i<size;i++)
		{
			if(arr[i]==fnum)
			count+=1;
		}
		if(count==0)
			printf("%d is not present in array\n",fnum);
		else
			printf("Frequency of %d in array : %d\n",fnum,count);
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
