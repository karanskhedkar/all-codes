/*Arranging an Array
- Rearrange the given array in-place such that all the negative numbers occur before
positive numbers.
- (Maintain the order of all -ve and +ve numbers as given in the original array).*/

#include<stdio.h>
void main()
{
	int size=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		printf("Enter elements of array\n");
		for (int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		printf("\nBefore rearranging : \n");
		for (int i=0;i<size;i++)
		{
			printf("%d ",arr[i]);
		}
		printf("\n");
		int x=0;
		int temp=0;
		for (int i=0;i<size;i++)
		{
			if(arr[i]<0)
			{
				for (int j=i;x<j;j--)
				{
					temp=arr[j];
					arr[j]=arr[j-1];
					arr[j-1]=temp;
				}
				x+=1;
			}
		}
		printf("\nAfter rearranging : \n");
		for (int i=0;i<size;i++)
		{
			printf("%d ",arr[i]);
		}
		printf("\n");
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
