/*Count Pair sum
- Given two sorted arrays(arr1[] and arr2[]) of size M and N of distinct elements.
- Given a value Sum.
- The problem is to count all pairs from both arrays whose sum is equal to Sum.*/

#include<stdio.h>
void main()
{
	int size1=0;
	int size2=0;
	printf("Enter size of array 1 : ");
	scanf("%d",&size1);
	printf("Enter size of array 2 : ");
	scanf("%d",&size2);
	if(size1>0 && size2>0)
	{
		int arr1[size1];
		int arr2[size2];
		printf("Enter elements of array 1\n");
		for(int i=0;i<size1;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr1[i]);
		}

		printf("Enter elements of array 2\n");
		for(int i=0;i<size2;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr2[i]);
		}
		int sum=0;
		printf("Enter value of sum : ");
		scanf("%d",&sum);
		int flag=0;
		for(int i=0;i<size1;i++)
		{
			for(int j=0;j<size2;j++)
			{
				if(arr1[i]+arr2[j]==sum)
				{
					flag=1;
					printf("arr1[%d] + arr2[%d] = %d\n",i,j,sum);
					printf("%d + %d = %d\n",arr1[i],arr2[j],sum);
				}
			}
		}
		if(flag==0)
			printf("No pair found\n");
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative");
}
