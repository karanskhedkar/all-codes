/*Maximum product of two numbers
- Given an array Arr of size N with all elements greater than or equal to zero.
- Return the maximum product of two numbers possible.*/

#include<stdio.h>
void main()
{
	int size=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>1)
	{
		int arr[size];
		printf("Enter elements of array\n");
		for(int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		int temp=0;
		int m1=0;
		int m2=0;
		int max=arr[0]*arr[1];
		for(int i=0;i<size;i++)
		{
			for(int j=i+1;j<size;j++)
			{
				if(arr[i]*arr[j]>=max)
				{
					m1=arr[i];
					m2=arr[j];
					max=m1*m2;
				}
			}
		}
		printf("Maximum product of two elements: %d * %d = %d\n",m1,m2,max);
	}
	else
		printf("Invalid input for size. Array size must be greater than or eqaul to 2 for finding maximum product of two numbes.\n");
}
