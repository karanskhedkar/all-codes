/*Count of smaller elements
- Find number of elements which are less than or equal to given element X.*/

#include<stdio.h>
void main()
{
	int size=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		int count=0;
		int x=0;
		printf("Enter elements of array\n");
		for(int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		printf("Enter value of X : ");
		scanf("%d",&x);
		for(int i=0;i<size;i++)
		{
			if(arr[i]<=x)
			{
				count++;
			}
		}
		printf("Number of element which are less than or equal to %d : %d\n",x,count);
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
