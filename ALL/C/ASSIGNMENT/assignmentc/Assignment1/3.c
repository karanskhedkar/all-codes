/*Maximum and Minimum
- Find the minimum and maximum elements in the array.*/

#include<stdio.h>
void main()
{
	int size=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		int max=0;
		int min=0;
		printf("Enter elements of array\n");
		for (int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
			if(i==0)
			{
				max=arr[i];
				min=arr[i];
			}
			else
			{
				if(arr[i]>max)
				{
					max=arr[i];
				}
				if(arr[i]<=min)
				{
					min=arr[i];
				}
			}
		}
		printf("Maximum element : %d\n",max);
		printf("Minimum element : %d\n",min);
	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
