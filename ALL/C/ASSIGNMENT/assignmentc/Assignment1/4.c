/*Search an Element in an array
- Find if the given element is present in array or not.*/

#include<stdio.h>
void main()
{
	int size=0;;
	int flag=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	if(size>0)
	{
		int arr[size];
		int ind;
		int fnum=0;
		int flag=0;
		printf("Enter elements of array \n");
		for (int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		printf("Enter element whose index value is required : ");
		scanf("%d",&fnum);
		for (int i=0;i<size;i++)
		{
			if(arr[i]==fnum)
			{
				printf("%d found at index : %d\n",fnum,i);
				flag=1;
				break;
			}
		}
		if(flag==0)
			printf("%d is not present in given array\n",fnum);

	}
	else
		printf("Invalid input for array size. Size cannot be zero or negative\n");
}
