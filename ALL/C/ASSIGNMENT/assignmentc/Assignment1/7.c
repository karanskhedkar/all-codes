/*Kth smallest element
- Given an array arr[] and an integer K where K is smaller than size of array, the task is
to find the Kth smallest element in the given array.
- It is given that all array elements are distinct.*/

#include<stdio.h>
void main()
{
	int size=0;
	int k=0;
	printf("Enter size of array : ");
	scanf("%d",&size);
	printf("Enter value of K : ");
	scanf("%d",&k);
	if(size>=k)
	{
		int arr[size];
		printf("Enter elements of array\n");
		for(int i=0;i<size;i++)
		{
			printf("%d element : ",i+1);
			scanf("%d",&arr[i]);
		}
		int temp=0;
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size-1-i;j++)
			{
				if(arr[j]>arr[j+1])
				{
					temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		printf("%d smallest number in array : %d\n",k,arr[k-1]);

	}
	else
		printf("Invalid Input for size. Size must be greater than or equal to %d\n",k);

}
