'''
PROGRAM TO TAKE POINTS AS INPUT AND PRINT RESPECTIVE RANKINK
GENIN : 0-100
CHUNIN : 100-500
JONIN : 500-1000
HOKAGE : 1000 AND ABOVE
'''
r=int(input("Enter your points : "))

if(0<=r and r<=100):
	print("Your rank is Genin")
elif(100<r and r<=500):
	print("Your rank is Chunin")
elif(500<r and r<=1000):
	print("Your rank is Jonin")
elif(1000<r):
	print("Your rank is Hokage")
else:
	print("Invalid Input")
