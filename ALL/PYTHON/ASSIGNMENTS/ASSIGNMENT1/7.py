'''
PROGRAM TO TAKE NUMBER IN RANGE(1-12) AND PRINT RESPECTIVE MONTH WITH NUMBER OF DAYS
'''
n=int(input("Enter day's number(1-12) : "))

if(n==1):
	print("January is a 31 days month")
elif(n==2):
	print("February is a 28/29 days month")
elif(n==3):
	print("March is a 31 days month")
elif(n==4):
	print("April is a 30 days month")
elif(n==5):
	print("May is a 31 days month")
elif(n==6):
	print("June is a 30 days month")
elif(n==7):
	print("July is a 31 days month")
elif(n==8):
	print("August is a 31 days month")
elif(n==9):
	print("September is a 30 days month")
elif(n==10):
	print("October is a 31 days month")
elif(n==11):
	print("November is a 30 days month")
elif(n==12):
	print("December is a 31 days month")
else:
	print("Invalid Input")
