'''
PROGRAM TO FIND WETHER ENTERED CHARACTER IS VOWEL OR CONSONANT
'''
n=input("Enter character : ")

if(('a'<=n and n<='z') or ('A'<=n and n<='Z')):
	if(n=='a' or n=='e' or n=='i' or n=='o' or n=='u' or n=='A' or n=='E' or n=='I' or n=='O' or n=='U'):
		print(n,"is a vowel")
	else:
		print(n,"is a consonant")
else:
	print(n,"is not alphabet")
