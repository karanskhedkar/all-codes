# Take integer elements in the array and make it a single-digit and add it by 2 and re-arrange it in the array.

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
num=0
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
	num=(num*10)+arr[i]
print('Original array :',arr)
num1=num+2
temp=num1
c=0
while(temp>0):
	c+=1
	temp=temp//10
arr=numpy.zeros(c,int)
i=1
while(c>0):
	rem=num1%10
	arr[c-1]=rem
	c-=1
	num1=num1//10
print('After adding 2 to the last element :',arr)
