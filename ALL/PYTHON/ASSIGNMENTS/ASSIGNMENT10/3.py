
# Write a program to sort array in both ascending and descending order

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
print('Original array :',arr)
for j in range(len(arr)):
	for k in range(len(arr)-1):
		if(arr[k]>arr[k+1]):
			temp=arr[k]
			arr[k]=arr[k+1]
			arr[k+1]=temp
print('Assending order :',arr)
for j in range(len(arr)):
	for k in range(len(arr)):
		if(arr[j]>arr[k]):
			temp=arr[k]
			arr[k]=arr[j]
			arr[j]=temp
print('Decending order :',arr)

