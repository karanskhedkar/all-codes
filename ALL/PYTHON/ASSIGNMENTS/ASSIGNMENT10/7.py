# Write a program to interchange or swap the consecutive even column index elements from 2-d 4x4 array
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
for i in range(len(arr)):
	for j in range(len(arr[i])):
		arr[i][j]=int(input('Enter number : '))
print('Original array :')
print(arr)
c1=int(input('Enter position of first column : '))
c2=int(input('Enter position of second column : '))
if((-1<c1 and c1<len(arr[0])+1) and (-1<c2 and c2<len(arr[0])+1)):
	for i in range(len(arr)):
		temp=arr[i][c1-1]
		arr[i][c1-1]=arr[i][c2-1]
		arr[i][c2-1]=temp
	print('After swapping',c1,'column with',c2,':')
	print(arr)
else:
	print('Invalid in put for position of column')
