# Write a program to reverse the 2-d array
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
for l in range(len(arr)):
	for m in range(len(arr[l])):
		arr[l][m]=int(input('Enter number : '))
print('Original array:')
print(arr)
if(r%2==1):
	r1=(r//2)+1
else:
	r1=(r//2)
for i in range(r1):
	for j in range(c):
		temp=arr[i][j]
		arr[i][j]=arr[r-1-i][c-1-j]
		arr[r-1-i][c-1-j]=temp
if(r%2==1):
	if(c%2==1):
		c1=(c//2)+1
	else:
		c1=(c//2)
	m=r//2
	for j in range(c1):
		temp=arr[m][j]
		arr[m][j]=arr[m][c-1-j]
		arr[m][c-1-j]=temp
print('After reversing :')
print(arr)
