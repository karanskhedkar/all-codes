# Write a program to find the target element and return its index from the array

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
target=int(input('Enter number whose index is required : '))
flag=0
for i in range(len(arr)):
	if(arr[i]==target):
		flag=1
		print('Index of',target,':',i)
		break
if(flag==0):
	print(target,'not present in the array')
