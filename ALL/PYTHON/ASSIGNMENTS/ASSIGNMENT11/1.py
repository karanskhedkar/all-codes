
#QUESTION : 1
# Given an array of nxn binary number matrix (0,1), You have to flip the bit and reverse the row

import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
flag=0
for i in range(len(arr)):
	for j in range(len(arr[i])):
		arr[i][j]=int(input('Enter number : '))
		if(arr[i][j]!=1 and arr[i][j]!=0):
			flag=1
			break
	if(flag==1):
		print(arr[i][j],'is not binary number. Program terminatted')
		break
if(flag==0):
	print('Original matrix:\n')
	print(arr)
	c=len(arr[0])//2

	for i in range(len(arr)):
		for j in range(c):
			temp=arr[i][j]
			arr[i][j]=arr[i][len(arr[i])-j-1]
			arr[i][len(arr[i])-j-1]=temp
	for i in range(len(arr)):
		for j in range(len(arr[i])):
			if(arr[i][j]==0):
				arr[i][j]=1
			else:
				arr[i][j]=0
	print('\nAfter reversing column and reversing the bit:\n')
	print(arr)

