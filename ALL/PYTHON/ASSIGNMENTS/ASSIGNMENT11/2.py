
#QUESTION : 2
# Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to the target

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(n):
	arr[i]=int(input('Enter number : '))
print()
print(arr)
t=int(input('\nEnter target number : '))
flag=0
for i in range(len(arr)):
	for j in range(i+1,len(arr)):
		if(arr[i]+arr[j]==t):
			flag=1
			print('\nAccording to index as follows')
			print('arr[',i,'] + arr[',j,'] = ',t)
			print('According to values present at that index')
			print(arr[i],'+',arr[j],'=',arr[i]+arr[j])
if(flag==0):
	print('\nNo pair of numbers found whose addition is',t)


