
#QUESTION : 3
# Given an array on n size. Find the kth smallest element from the array

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
print()
print(arr)
nth=int(input('\nEnter nth smallest element to find : '))
if(nth>0 and nth<len(arr)+1):
	for i in range(len(arr)):
		for j in range(len(arr)-1):
			if(arr[j]>arr[j+1]):
				temp=arr[j]
				arr[j]=arr[j+1]
				arr[j+1]=temp
	print()
	print(nth,'smallest element :',arr[nth-1])
else:
	print('Total number of elements present in array :',len(arr))
	print('nth smallest number to find :',nth)
	print('Hence invalid input')


