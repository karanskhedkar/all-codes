
#QUESTION : 4
# Given an array, rotate the array to the right by k steps, where k is non-negative.

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
k=int(input('\nEnter number of times to rotate : '))
if(k>-1):
	print('\nBefore rotating :')
	print(arr)
	for j in range(k):
		temp=arr[len(arr)-1]
		for k in range(len(arr)-1):
			arr[len(arr)-k-1]=arr[len(arr)-k-2]
		arr[0]=temp
	print('\nAfter rotating :')
	print(arr)
else:
	print('Entered value of how many times to rotate :',k)
	print('Please enter a positive value')


