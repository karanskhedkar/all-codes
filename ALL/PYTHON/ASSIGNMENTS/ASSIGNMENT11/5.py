
#QUESTION : 5
# Find the single element from the given array

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
flag=0
for i in range(len(arr)):
	c=0
	for j in range(len(arr)):
		if(arr[i]==arr[j]):
			c+=1
	if(c==1):
		print(arr[i],end=' ')
		flag=1
if(flag==0):
	print('\nNo single element found')
else:
	print()


