# You have a long flowerbed in which some of the plots are planted, and some are not. However, flowers cannot be planted in adjacent plots.

import numpy
n=int(input('Enter size of flower bed : '))
print('Enter 1 for planted and 0 for empty')
flag=0
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
	if(arr[i]!=1 and arr[i]!=0):
		flag=1
		break
if(flag==1):
	print('Enter 1 or 0 only',arr[i],'is invalid.')
	print('Program terminated')
else:
	count=0
	flag=0
	p=int(input('Enter number of plants to plant : '))
	for i in range(len(arr)):
		if(i==0 or i==len(arr)-1):
			continue
		elif(arr[i]==0 and arr[i-1]==0 and arr[i+1]==0):
			flag=1
			count+=1
	if(count==0 or count!=p):
		print('Not possible')
	else:
		print('Planting is possible')
