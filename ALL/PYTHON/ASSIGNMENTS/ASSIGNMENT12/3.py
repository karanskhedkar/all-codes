# Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
m=arr[0]
for i in range(len(arr)):
	for j in range(i,len(arr)):
		add=0
		for k in range(i,j+1):
			add=add+arr[k]
		if(add>m):
			m=add
print(m)