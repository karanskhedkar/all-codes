# Given an integer numRows, return the first numRows of Pascal's triangle.

import numpy
n=int(input('Enter number till which pascal triangle is required : '))
if(n>0):
	copy=numpy.zeros(n,int)
	print('[',end='')
	for i in range(1,n+1):
		x=0
		if(i==1 or i==2):
			arr=numpy.ones(i,int)
			for k in range(len(arr)):
				copy[k]=arr[k]
			if(i<n):
				print(arr,end=',')
			else:
				print(arr,end=']\n')
		else:
			arr=numpy.zeros(i,int)
			for j in range(len(arr)):
				if(j==0 or j==len(arr)-1):
					arr[j]=1
				else:
					arr[j]=copy[x]+copy[x+1]
					x+=1
			for k in range(len(arr)):
				copy[k]=arr[k]
			if(i<n):
				print(arr,end=',')
			else:
				print(arr,end=']\n')

'''
# Pattern of Pascal Triangle

import numpy

n=int(input("Enter Rows : "))
arr=numpy.zeros((n,n),int)

for i in range(n):
    for j in range(i+1):
        if j==0 or j==i:
            print("1",end="  ")
            arr[i][j]=1
        else:
            print(arr[i-1][j]+arr[i-1][j-1],end='  ')
            arr[i][j]=arr[i-1][j]+arr[i-1][j-1]
    print()
    
print('\n',arr)'''