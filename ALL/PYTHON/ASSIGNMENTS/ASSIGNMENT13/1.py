# Given an integer array nums, find a contiguous non-empty subarray within the array that has the largest product, and return the product.

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
maximum=arr[0]
for i in range(len(arr)):
	for j in range(i,len(arr)):
		mult=1
		for k in range(i,j+1):
			mult=arr[k]*mult
		if(mult>=maximum):
			maximum=mult
print(maximum)