# Given an array of integers nums, sort the array in ascending order.

import numpy
n=int(input('Enter size of array : '))
arr=numpy.zeros(n,int)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
print('Before sorting : ')
print(arr)
print('After sorting : ')

for i in range(len(arr)-1):
	for j in range(len(arr)-i-1):
		if(arr[j]>arr[j+1]):
			temp=arr[j]
			arr[j]=arr[j+1]
			arr[j+1]=temp
print(arr)