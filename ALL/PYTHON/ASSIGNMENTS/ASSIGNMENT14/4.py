# You have a set of integers s, which originally contains all the numbers from 1 to n. Unfortunately, due to some error,
# one of the numbers in s got duplicated to another number in the set, which results in repetition of one number and loss of another number.

import numpy
n=int(input('Enter values of n : '))
arr=numpy.zeros(n,int)
print('Enter numbers between 1 -',n)
for i in range(len(arr)):
	arr[i]=int(input('Enter number : '))
	if(arr[i]<=0 or n+1<=arr[i]):
		print(arr[i],'does not lies between 1 -',n)
		print('Program terminated')
		break
flag=0
for i in range(1,len(arr)+1):
	c=0
	for j in range(len(arr)):
		if(i==arr[j]):
			c+=1
	if(c==2):
		flag=1
		d=i
	elif(c==0):
		m=i
if(flag==0):
	print('No duplicate found')
else:
	print('Repeated element :',d)
	print('Missing element :',m)


