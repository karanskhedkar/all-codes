"""
SUM OF ALL N DIGIT NUMBERS
"""
n=int(input("Enter number of digits : "))
s=0
for i in range(pow(10,n-1),pow(10,n)):
	s=s+i
print("Sum of all",n,"digit numbers :",s)
