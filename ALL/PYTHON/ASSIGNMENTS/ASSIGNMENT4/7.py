c=input("Enter any character : ")

if(('a'<=c and c<='z') or ('A'<=c and c<='Z')):
	print(c,"is a alphabet")
elif('0'<=c and c<='9'):
	print(c,"is a digit")
elif((33<=ord(c) and ord(c)<=47) or (58<=ord(c) and ord(c)<=64) or (91<=ord(c) and ord(c)<=96) or (123<=ord(c) and ord(c)<=126)):
	print(c,"is a special character")
else:
	print(c,"is not a alphabet,digit or special character")