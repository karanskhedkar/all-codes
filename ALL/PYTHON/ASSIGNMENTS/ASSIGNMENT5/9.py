r=int(input("Enter number of rows : "))

for i in range(r):

	for j in range(r-i-1):
		print(' ',end=' ')


	num=1
	for k in range(i+1):
		if(i%2==0):
			print(chr(num+96),end=' ')

		else:
			print(num,end=' ')
		num+=1
	print()