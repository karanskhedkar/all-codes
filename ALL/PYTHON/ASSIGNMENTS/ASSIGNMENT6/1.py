# Write a program to find even numbers from the array

import array

arr=array.array('i',[])

num=int(input('Enter number of element to be entered : '))
flag=0
if(num>0):
	for i in range(num):
		n=int(input('Enter number : '))
		arr.append(n)
	print()
	print('Even numbers in',arr,'are as follow : ')
	for i in range(len(arr)):
		if(arr[i]%2==0):
			flag=1
			print(arr[i],end=' ')
	print()
	if(flag==0):
		print('No even number found')
elif(num==0):
	print('Empty array')
else:
	print('INVALID INPUT')

