# Write a program to find prime number from the given array

import array

arr=array.array('i',[])

num=int(input('Enter number of elements to be entered : '))

for i in range(num):
	n=int(input('Enter element : '))
	arr.append(n)

for i in range(len(arr)):
	if(arr[i]<0):
		continue
	
	elif(arr[i]==0 or arr[i]==1):
		print(arr[i],'is not prime nor composite')
	
	else:
		count=0
		for j in range(2,arr[i]):
			if(arr[i]%j==0):
				count+=1
		if(count==0):
			print(arr[i],end=' ')
print()