# Write a program to find the occurrence of a given number from

import array

arr=array.array('i',[])

num=int(input('Enter number of element to be entered : '))
for i in range(num):
	n=int(input('Enter number : '))
	arr.append(n)

fnum=int(input('Enter number whose count is required : '))
count=0
for i in range(len(arr)):  #IT CAN BE ALSO FOUND USING count() METHOD
	if(arr[i]==fnum):
		count+=1
print('Count of',fnum,'=',count)