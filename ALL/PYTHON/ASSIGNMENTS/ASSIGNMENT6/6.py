# Write a program to find min and max from the given array

import array

arr=array.array('i',[])

num=int(input('Enter number of elements to be entered : '))

for i in range(num):
	n=int(input('Enter number : '))
	arr.append(n)

maximum=arr[0]
for i in range(1,len(arr)):
	if(arr[i]>maximum):
		maximum=arr[i]
print('Maximum number = ',maximum)

minimum=arr[0]
for i in range(1,len(arr)):
	if(arr[i]<minimum):
		minimum=arr[i]
print('Minimum number = ',minimum)

