# Write a program to take two 1-D character array from user and compare elements at the same indexes and print positive differences between them.


import array

arr1=array.array('u',[])
arr2=array.array('u',[])
num1=int(input('Enter number of elements to be entered in array 1 : '))
for i in range(num1):
	ele1=input('Enter element : ')
	arr1.append(ele1)
num2=int(input('Enter number of elements to be entered in array 2 : '))
for j in range(num2):
	ele2=input('Enter element : ')
	arr2.append(ele2)

if(num1==num2):
	for k in range(num1):
		if(ord(arr1[k])>ord(arr2[k])):
			n=ord(arr1[k])-ord(arr2[k])
			print('Difference of elements at index',k,':',arr1[k],'-',arr2[k],'=',n)
		else:		
			n=ord(arr2[k])-ord(arr1[k])
			print('Difference of elements at index',k,':',arr2[k],'-',arr1[k],'=',n)
else:
	print('Both array have different size. Hence cannot find differences')

