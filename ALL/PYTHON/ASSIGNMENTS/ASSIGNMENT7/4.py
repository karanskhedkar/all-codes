# Write a program to take a 1-D array from user and calculate cube of elements at even indexes and square of elements at odd indexes and reflect those calculations into an array.


import array

arr=array.array('i',[])
res=array.array('i',[])
num=int(input('Enter number of elements : '))

for i in range(num):
	ele=int(input('Enter element : '))
	arr.append(ele)

	if(i%2==0):
		res.append(ele**3)
	else:
		res.append(ele**2)
print(arr)
print(res)
