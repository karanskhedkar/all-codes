# Write a program to merge two given arrays.


import array

arr1=array.array('i',[])
arr2=array.array('i',[])
merge=array.array('i',[])

num1=int(input('Enter number of elements to be entered in array 1 : '))
for i in range(num1):
	ele1=int(input('Enter element : '))
	arr1.append(ele1)
	merge.append(ele1)

num2=int(input('Enter number of elements to be entered in array 2 : '))
for i in range(num2):
	ele2=int(input('Enter element : '))
	arr2.append(ele2)
	merge.append(ele2)

print('First array :',arr1)
print('Second array :',arr2)
print('Merged array :',merge)
