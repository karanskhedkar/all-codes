# Write a program to create an array of ‘n’ integer elements. Where ‘n’ value should be taken from the user. Insert the values from users and find the prime numbers from the array


import array

arr=array.array('i',[])
prime=[]
num=int(input('Enter number of elements : '))

for i in range(num):
	ele=int(input('Enter element : '))
	arr.append(ele)
for i in range(len(arr)):

	if(arr[i]<2):
		continue
	
	else:
		count=0
		for j in range(2,arr[i]):
			if(arr[i]%j==0):
				count+=1
		if(count==0):
			prime.append(arr[i])
if(len(prime)==0):
	print('No prime number found')
else:
	print('Prime numbers present in array : ',prime)


