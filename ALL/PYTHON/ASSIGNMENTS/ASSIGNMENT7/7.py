# Write a program to create an array of ‘n’ integer elements. Where ‘n’ value should be taken from the user. Insert the values from the user and find the strong number from them

import array

arr=array.array('i',[])
strong=[]
num=int(input('Enter number of elements : '))

for i in range(num):
	ele=int(input('Enter element : '))
	arr.append(ele)

for k in range(num):
	temp=arr[k]
	add=0
	while(temp>0):
		rem=temp%10
		fact=1
		for j in range(1,(rem+1)):
			fact=fact*j
		add=add+fact
		temp=temp//10
	if(add==arr[k]):
		strong.append(arr[k])
if(len(strong)==0):
	print('No strong number found')
else:
	print('Strong numbers present in array : ',strong)
