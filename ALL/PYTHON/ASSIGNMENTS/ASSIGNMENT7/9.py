# Write a program to take two 1-D array from user and calculate addition of elements at the same indexes from both array and store that sum in the third array and print all three arrays.


import array

arr1=array.array('i',[])
arr2=array.array('i',[])
add=array.array('i',[])
num1=int(input('Enter number of elements to be entered in array 1 : '))
for i in range(num1):
	ele1=int(input('Enter element : '))
	arr1.append(ele1)
num2=int(input('Enter number of elements to be entered in array 2 : '))
for j in range(num2):
	ele2=int(input('Enter element : '))
	arr2.append(ele2)

if(num1==num2):
	for k in range(num1):
		n=arr1[k]+arr2[k]
		add.append(n)
	print('Array 1 =',arr1)
	print('Array 2 =',arr2)
	print('Array 1 + Array 2 = ',add)
else:
	print('Both array have different size. Hence cannot carry out addition')

