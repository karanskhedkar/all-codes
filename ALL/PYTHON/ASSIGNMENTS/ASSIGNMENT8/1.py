# WAP to convert a 1D array into 2D array
import numpy
n=int(input('Enter number of elements in 1D array : '))
arr1 = numpy.zeros(n,int)
for i in range(n):
	ele=int(input('Enter element : '))
	arr1[i]=ele
print(arr1)
r=int(input('Enter number of rows in 2D array : '))
c=int(input('Enter number of columns in 2D array : '))
arr2=numpy.zeros((r,c),int)
if((r*c)==n):
	count=0
	for i in range(r):
		for j in range(c):
			arr2[i][j]=arr1[count]
			count+=1
	print('Before coverting : ')
	print(arr1)
	print()
	print('After coverting : ')
	print(arr2)
else:
	print('Size of 1D Array : ',len(arr1))
	print('Size of 2D Array : ',len(arr2)*len(arr2))
	print('Size mismatched. Cannot convert')
