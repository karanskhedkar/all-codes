# WAP to convert a 2D array into 1D array
import numpy
r=int(input('Enter number of rows in 2D array : '))
c=int(input('Enter number of columns in 2D array : '))
arr2=numpy.zeros((r,c),int)
for i in range(r):
		for j in range(c):
			ele=int(input('Enter element : '))
			arr2[i][j]=ele
n=int(input('Enter number of elements in 1D array : '))
arr1 = numpy.zeros(n,int)
if((r*c)==n):
	count=0
	for i in range(r):
		for j in range(c):
			arr1[count]=arr2[i][j]
			count+=1
	print('Before coverting : ')
	print(arr2)
	print('After coverting : ')
	print(arr1)
else:
	print('Size of 1D Array : ',len(arr1))
	print('Size of 2D Array : ',len(arr2)*len(arr2))
	print('Size mismatched. Cannot convert')
