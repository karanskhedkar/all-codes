# WAP to take input from the user into an array and remove duplicate numbers
import array
n=int(input('Enter number of elements to be entered in 1D array : '))
arr = array.array('i')
for i in range(n):
	ele=int(input('Enter element : '))
	arr.append(ele)
print(arr)
count=0
for i in range(len(arr)):
	for j in range(len(arr)):
		if(i==j):
			continue
		elif(arr[i]==arr[j]):
			for k in range(j,len(arr)-1):
				arr[k]=arr[k+1]
for i in range(len(arr)):
	if(arr[len(arr)-1]==arr[i]):
		count+=1
for i in range(1,count):
	arr.pop()
print(arr)
