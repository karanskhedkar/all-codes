# Write a java program to take a 2-D array of order 3 X 3 and check whether that matrix is an identity matrix or not
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of rows : '))
if(r==c):
	arr=numpy.zeros((r,c),int)
	count1=0
	count0=0
	for i in range(len(arr)):
		for j in range(len(arr[0])):
			ele=int(input('Enter element : '))
			arr[i][j]=ele

			if(i==j and ele==1):
				count1+=1
			elif(ele==0):
				count0+=1
	if(count1==r and count0==(r*c)-r):
		print(arr)
		print('Is a identity matrix')
	else:
		print(arr)
		print('Not a identity matrix')
else:
	print('No of rows = ',r)
	print('No of columns = ',c)
	print('Not a square matrix. Identity matrix must be a square matrix')
