# Write a java program to take a 2-D array of order 3 X 3 and swap 1st row with 3rd row and print it as before and after operation.
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
for i in range(len(arr)):
	for j in range(len(arr[i])):
		ele=int(input('Enter element : '))
		arr[i][j]=ele
r1=int(input('Enter row number of first row for swapping : '))
r2=int(input('Enter row number of second row for swapping : '))
i1=r1-1
i2=r2-1
if(i1<len(arr) and i2<len(arr)):
	print('Before swapping row',r1,'with',r2)
	print(arr)
	for j in range(len(arr[0])):
		temp=arr[i1][j]
		arr[i1][j]=arr[i2][j]
		arr[i2][j]=temp
	print('After swapping row',r1,'with',r2)
	print(arr)
else:
	print('Invalid input for number of rows to swap')
