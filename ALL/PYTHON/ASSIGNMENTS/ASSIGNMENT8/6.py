# WAP to find the third largest element from an array
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
for i in range(r):
    for j in range(c):
        ele=int(input('Enter element : '))
        arr[i]=ele
m1=arr[0][0]
m2=arr[0][0]
m3=arr[0][0]
for i in range(len(arr)):
    for j in range(len(arr[i])):
	    if(arr[i][j]>m1):
		    m3=m2
		    m2=m1
		    m1=arr[i][j]
	    if(arr[i][j]<m1 and arr[i][j]>m2):
		    m3=m2
		    m2=arr[i][j]
	    if(arr[i][j]<m2 and arr[i][j]>m3):
		    m3=arr[i][j]
print('Third largest element : ',m3)
