# Write a java program to take a 2-D array of order 3 X 3 and Sort that array in ascending order and print it as before and after operation.
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))
arr=numpy.zeros((r,c),int)
for i in range(r):
	for j in range(c):
		ele=int(input('Enter element : '))
		arr[i][j]=ele
print()
print('Before swapping')
print(arr)
for a in range(r):
	for b in range(c):
       
		for i in range(r):
			for j in range(c):
				
				if(arr[a][b]<arr[i][j]):
					num=arr[i][j]
					arr[i][j]=arr[a][b]
					arr[a][b]=num
print()
print('After swapping')
print(arr)
