#Write a program to create a 2d array of integer elements. Take the number of rows and columns values from the user. And print a 2d array of numbers whose first digit is N, take the N value from the user.
#Input:
#Enter number of Rows = 2
#Enter number of Column = 2
#Enter value of N = 3
#Output:
#3 30
#31 32
import numpy
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columnss : '))
n=int(input('Enter value of N : '))
arr=numpy.zeros((r,c),int)
count=-1
for i in range(len(arr)):
	for j in range(len(arr[i])):
		if(i==j==0):
			arr[i][j]=n
		else:
			count+=1
			arr[i][j]=(n*10)+count
print(arr)
