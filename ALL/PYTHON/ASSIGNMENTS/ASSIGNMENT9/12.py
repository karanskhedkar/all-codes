# Write a program which toggles the case of a string.

str1=input('Enter string : ')
str2=''
for i in range(len(str1)):
	if('A'<=str1[i] and str1[i]<='Z'):
		str2=str2+chr(ord(str1[i])+32)
	elif('a'<=str1[i] and str1[i]<='z'):
		str2=str2+chr(ord(str1[i])-32)
	else:
		str2=str2+str1[i]
print('Before converting :',str1)
print('After converting :',str2)
