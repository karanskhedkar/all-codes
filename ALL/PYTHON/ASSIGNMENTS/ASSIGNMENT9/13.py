# Write a program to check whether given strings are Anagram strings or not.

str1=input('Enter First string : ')
str2=input('Enter Second string : ')
str3=str1
c=0
if(len(str1)==len(str2)):
	for i in range(len(str3)):
		c1=0
		c2=0
		for j in range(len(str1)):
			if(str3[i]==str1[j]):
				c1+=1
			if(str3[i]==str2[j]):
				c2+=1
		if(c1==c2):
			c+=1
	if(c==len(str1)):
		print(str1,'and',str2,' are anagram')
	else:
		print(str1,'and',str2,' are not anagram')
else:
	print('String sizes are different. Hence not anagram')
