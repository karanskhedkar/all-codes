# Write a program which accepts strings from the user and accept number N then copy the last N character into some other string.

str1=input('Enter string : ')
str2=''
n=int(input('How many chracters from ending do you want to copy : '))
if(0<=n and n<=len(str1)):
	for i in range(len(str1)-n,len(str1)):
		str2=str2+str1[i]
	print('Original string :',str1)
	print('Last',n,'characters :',str2)
else:
	print('Invalid Input for N')
