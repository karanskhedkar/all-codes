# Write a program which accepts two strings from the user and compare two strings without case sensitivity. If both strings are equal then return 0 otherwise return difference between first mismatch character.

str1=input('Enter first string : ')
str2=input('Enter second string : ')
count=0

if(len(str1)==len(str2)):
	
	for i in range(len(str1)):
		diff=ord(str1[i])-ord(str2[i])
		if((diff==0) or (diff==-32) or (diff==32)):
			count+=1
		else:
			if(str1[i]>str2[i]):
				print('Difference between',str1[i],'and',str2[i],':',ord(str1[i])-ord(str2[i]))
			else:
				print('Difference between',str2[i],'and',str1[i],':',ord(str2[i])-ord(str1[i]))
			break
	if(count==len(str1)):
		print('Strings are equal : 0')

else:
	print('Strings are of different size. Hence cannot be compared')
