# Write a program which accepts string from the user and then reverse the string till first N characters without taking another string.

str1=input('Enter string : ')
str2=''
n=int(input('Enter number of charasters to be reversed from starting : '))
str2=''
i=n-1
if(n<=len(str1)):
	for i in range(i,-1,-1):
		str2=str2+str1[i]
	for k in range(n,len(str1)):
		str2=str2+str1[k]
print('Original string : ',str1)
print('After reversing first',n,'characters :',str2)
