# Write a program which accepts string from the user and then reverse the string till the last N characters without taking another string.

str1=input('Enter string : ')
str2=''
n=int(input('Enter number of charasters to be reversed from starting : '))
str2=''
if(n<=len(str1)):
	for i in range(len(str1)-n):
		str2=str2+str1[i]
	for k in range(len(str1)-1,len(str1)-n-1,-1):
		str2=str2+str1[k]
print('Original string : ',str1)
print('After reversing last',n,'characters :',str2)
