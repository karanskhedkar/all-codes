# Write a program which accepts strings from the user and then accepts a range and reverse the string in that range without taking another string

str1=input('Enter string : ')
str2=''
n1=int(input('Enter starting point : '))
n2=int(input('Enter ending point : '))
str2=''
i1=n1-1
i2=n2-1
if(n1<=n2 and n1<=len(str1) and n2<=len(str1)):
	for i in range(i1):
		str2=str2+str1[i]

	for j in range(i2,i1-1,-1):
		str2=str2+str1[j]

	for k in range(i2+1,len(str1)):
		str2=str2+str1[k]
print('Original string :',str1)
print('String after reversing characters between',n1,'and',n2,':',str2)
