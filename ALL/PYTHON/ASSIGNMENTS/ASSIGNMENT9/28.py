# Write a program which accepts strings from the user and check whether the string is palindrome or not.

str1=input('Enter string : ')
count=0

for i in range(len(str1)//2):
	if(str1[i]==str1[len(str1)-i-1]):
		count+=1
if(count==len(str1)//2):
	print('Before reversing : ',str1[0::])
	print('After reversing : ',str1[-1:-len(str1)-1:-1])
	print('Hence palindrome')
else:
	print('Before reversing :',str1[0::])
	print('After reversing :',str1[-1:-len(str1)-1:-1])
	print('Hence, not palindrome')
