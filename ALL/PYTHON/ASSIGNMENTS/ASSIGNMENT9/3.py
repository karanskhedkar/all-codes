# Write a program which accepts sentences from the user and print a number of small letters, capital letters and digits from that sentence.

str1=input('Enter string : ')
str2=''
cl=0
sl=0
dg=0

for i in range(len(str1)):
	if('A'<=str1[i] and str1[i]<='Z'):
		cl+=1

	elif('a'<=str1[i] and str1[i]<='z'):
		sl+=1

	elif('0'<=str1[i] and str1[i]<='9'):
		dg+=1

print('Total capital alphabets : ',cl)
print('Total small alphabets : ',sl)
print('Total digits : ',dg)
