# Write a program which sets first N characters in string to a specific character

str1=input('Enter string : ')
char=input('Enter character : ')
N=int(input('Enter number of characters from starting : '))
temp=str1
str1=''
if(0<=N and N<=len(temp)):
	for i in range(len(temp)):
		if(i<N):
			str1=str1+char
		else:
			str1=str1+temp[i]

	print('Original string :',temp)
	print('After replacing first',N,'characters with',char,':',str1)
else:
	print('Invalid input for N')
