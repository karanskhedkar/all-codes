# Write a program which accepts string from user and search last occurrence of specific character in string and return the position at which character is found

str1=input('Enter string : ')
char=input('Enter character whose last position is required : ')
index=-1
for i in range(len(str1)):
	if(str1[i]==char):
		index=i
if(index==-1):
	print(char,'is not present in string')
else:
	print('Last position of',char,'is',':',index)
