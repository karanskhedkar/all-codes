# Write a program which accepts sentences from the user and print a number of words of even and odd length from that sentence.

str1=input('Enter string : ')
even=0
odd=0
str2=''
count=0
for i in range(len(str1)):
	if(str1[i]!=' '):
		str2=str2+str1[i]
		count+=1
	else:
		if(count%2==0):
			even+=1
		else:
			odd+=1
		count=0
		str2=''
if(count%2==0):
	even+=1
else:
	odd+=1
print('Total word with odd number of alphabets : ',odd)
print('Total word with even number of alphabets : ',even)
