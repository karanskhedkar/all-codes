'''
2  5  10  17
  26  37  50
      65  81
          101
'''
r=int(input('Enter number of rows : '))
num=1
for i in range(r):

	for j in range(i):
		print(' ',end='  ')

	for k in range(r-i):
		print((num*num)+1,end='  ')
		num+=1
	print()
