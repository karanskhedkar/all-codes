'''WAP TO PRINT FOLLOWING PATTERN
*       * 
         
    *
       
*       *
'''

r=int(input('Enter number of rows : '))

for i in range(r):
  for j in range(r):

    if((i==0 and (j==0 or j==r-1)) or (i==r-1 and (j==0 or j==r-1)) or (2*i==r-1 and 2*j==r-1)):
      print('*',end=' ')
    else:
      print(' ',end=' ')
  print()
