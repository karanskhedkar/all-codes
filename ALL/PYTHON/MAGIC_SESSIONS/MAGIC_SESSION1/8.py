'''WAP TO PRINT FOLLOWING PATTERN
A B D G
  G H J
    J K
      K
'''
r=int(input('Enter number of rows : '))
alpha=65
for i in range(r):

    for j in range(i):
        print(' ',end=' ')

    for k in range(r-i):
        alpha=alpha+k
        print(chr(alpha),end=' ')
    print()