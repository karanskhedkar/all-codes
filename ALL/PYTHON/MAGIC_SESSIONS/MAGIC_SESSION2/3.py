l=int(input('Enter lower limit : '))
u=int(input('Enter upper limit : '))

if(0<=l and l<2):
	print('Lower limit cannot be less tahn 2, since 0 and 1 are neither prime nor composite')

else:
	for i in range(l,u+1):
		count=0
		for j in range(1,i+1):
			if(i%j==0):
				count+=1
		if(count<3):
			print(i,end=' ')
	print()
