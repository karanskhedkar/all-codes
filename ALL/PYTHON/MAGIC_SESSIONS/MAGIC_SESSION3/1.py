# WAP TO PRINT FACTORIAL ALL NUMBERS IN A GIVEN RANGE

l=int(input('Enter lower limit : '))
u=int(input('Enter upper limit : '))

if(0<=l and 0<=u):
	for i in range(l,u+1):
		fact=1
		for j in range(1,i+1):
			fact=fact*j
		print('Factorial of',i,'=',fact)
else:
	print('Negetive numbers cant be accepted as input, since negetive numbers dont have factorial')
