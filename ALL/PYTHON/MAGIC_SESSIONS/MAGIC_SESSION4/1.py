'''
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
'''
r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))
for i in range(r):
	num=i+1

	for j in range(c):
		print(num,end=' ')
		num+=1
	print()