'''
 0 2 4
 2 4 6
 4 6 8
 '''
r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))

for i in range(r):
	num=i*2
	for j in range(r):
		print(num,end=' ')
		num+=2
	print()