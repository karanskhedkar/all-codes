'''
1 2 3 4
A B C D
1 2 3 4
A B C D
'''
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))

for i in range(r):
	num=1

	for j in range(c):
		if(i%2==0):
			print(num,end=' ')
		else:
			print(chr(num+64),end=' ')
		num+=1
	print()
