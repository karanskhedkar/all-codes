'''
A B C D
E F G H
I J K L
M N O P
'''
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))

alpha=65

for i in range(r):
	for j in range(c):
		print(chr(alpha),end=' ')
		alpha+=1
	print()
