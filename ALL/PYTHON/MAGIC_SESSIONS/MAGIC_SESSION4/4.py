'''
1 2 3 4
A B C D
* * * *
5 6 7 8
'''
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))

num=1
alpha=65

for i in range(r):

	for j in range(c):

		 if(i%3==0):
		 	print(num,end=' ')
		 	num+=1
		 elif(i%3==1):
		 	print(chr(alpha),end=' ')
		 	alpha+=1
		 else:
		 	print('*',end=' ')
	print()