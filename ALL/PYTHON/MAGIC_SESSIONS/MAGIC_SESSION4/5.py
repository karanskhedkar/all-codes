'''
A B C D
F G H I 
K L M N
P Q R S
'''
r=int(input('Enter number of rows : '))
c=int(input('Enter number of columns : '))

alpha=65

for i in range(r):

	for j in range(c):

		print(chr(alpha),end=' ')
		alpha+=1
	print()
	alpha+=1