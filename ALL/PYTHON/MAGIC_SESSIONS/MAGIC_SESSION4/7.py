'''
1A 2B 3C
4D 5E 6F
7G 8H 9I
'''
r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))

num=1
for i in range(r):
	for j in range(c):
		print(num,end='')
		print(chr(num+64),end=' ')
		num+=1
	print()