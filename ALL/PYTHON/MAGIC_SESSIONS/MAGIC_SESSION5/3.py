# Write a Program that accepts a number from the user and prints the minimum digit from that number.

num=int(input('Enter number : '))
n1=num%10
temp=num
while(temp>0):
	rem=temp%10
	if(rem<n1):
		n1=rem
	temp//=10
print('Smallest digit in',num,'=',n1)