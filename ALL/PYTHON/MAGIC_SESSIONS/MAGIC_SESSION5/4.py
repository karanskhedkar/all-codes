#  Write a Program to Print following Pattern.
# 0 0 0 0 0
# 1 2 3 4
# 4 6 8
# 9 12
# 16
r=int(input('Enter number of rows : '))

for i in range(r):

	for j in range(i):
		print(' ',end=' ')

	num=i*i

	for k in range(r-i):
		print(num,end=' ')
		num+=i
	print()

