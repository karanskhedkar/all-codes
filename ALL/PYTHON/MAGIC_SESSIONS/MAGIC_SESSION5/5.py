# 5: Write a Program calculates slope of a line if the user provides the
# Two Points A & B of that line.
# {Note: Slope of a line is computed as m = ((y2-y1)/(x2-x1)) }
# Input:
# Point A (x1, y1) = 5 2
# Point B (x2, y2) = 8 1
# Output: Slope of line AB is 3

x1=int(input('Enter x coordinate of point 1 : '))
y1=int(input('Enter y coordinate of point 1 : '))
x2=int(input('Enter x coordinate of point 2 : '))
y2=int(input('Enter y coordinate of point 2 : '))

m=((y2-y1)/(x2-x1))

if(m<0):
	m=m*(-1)

m=int(1/m)

print(m)
