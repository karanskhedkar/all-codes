''''
5 4 3 2 1 2 3 4 5
  4 3 2 1 2 3 4
    3 2 1 2 3
      2 1 2
        1 
'''
r=int(input("Enter number of rows : "))

for i in range(r):

	for j in range(i):
		print(' ',end=' ')
	num=r-i

	for k in range(2*(r-i)-1):
		if(k<(r-i-1)):
			print(num,end=' ')
			num-=1
		else:
			print(num,end=' ')
			num+=1
	print()
