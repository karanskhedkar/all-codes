'''
* * *
A B C
1 2 3
* * *
D E F
4 5 6
'''
r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))
num=1
alpha=65
for i in range(r):

	for j in range(c):
		if(i%3==0):
			print("*",end=' ')
		if(i%3==1):
			print(chr(alpha),end=' ')
			alpha+=1
		if(i%3==2):
			print(num,end=' ')
			num+=1
	print()