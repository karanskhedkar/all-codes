r=int(input("Enter number of rows : "))

for i in range(r):
	for j in range(r):
		if((i==0 and j==0) or(j==r-1 and i==r-1) or (j==r-1 and i==0) or (i==r-1 and j==0) or(j==(r//2) and i==(r//2))):
			print("*",end=' ')
		else:
			print(' ',end=' ')
	print()