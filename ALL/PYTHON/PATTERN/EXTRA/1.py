'''
4 3 2 1
3 2 1
2 1
1
2 1
3 2 1
4 3 2 1
'''

r=int(input('Enter rows : '))
for i in range(r):
	n1=(r//2)+1-i
	n2=i-(r//2)+1
	if(i<(r//2)+1):
		for j in range((r//2)+1-i):
			print(n1,end=' ')
			n1-=1
	else:
		for j in range(i-(r//2)+1):
			print(n2,end=' ')
			n2-=1
	print()