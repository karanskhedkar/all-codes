'''
# # # #
* * *
# #
*
# #
* * *
# # # #
'''
r=int(input('Enter number of rows : '))

for i in range((r//2)+1):
	for j in range((r//2)+1-i):
		if(i%2==0):
			print('#',end=' ')
		else:
			print('*',end=' ')
	print()

for k in range(r//2):
	for l in range(k+2):
		if(k%2==0):
			print('#',end=' ')
		else:
			print('*',end=' ')
	print()
