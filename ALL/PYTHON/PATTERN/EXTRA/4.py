'''
1 2 3 4
1 2 3
1 2
1
1 2
1 2 3
1 2 3 4
'''
r=int(input('Enter number of rows : '))

for i in range((r//2)+1):
	num=1
	for j in range((r//2)+1-i):
		print(num,end=' ')
		num+=1
	print()

for k in range(r//2):
	num=1
	for l in range(k+2):
		print(num,end=' ')
		num+=1
	print()