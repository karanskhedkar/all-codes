'''
1   2   3   4
    5   6   7
        8   9
           10
       11  12
   13  14  15
16 17  18  19
'''
r=int(input('Enter rows : '))
n=1
for i in range((r//2)+1):
	for j in range(i):
		print(' ',end='\t')
	for k in range((r//2)+1-i):
		print(n,end='\t')
		n+=1
	print()

for i in range((r//2)):
	for j in range((r//2)-i-1):
		print(' ',end='\t')
	for k in range(i+2):
		print(n,end='\t')
		n+=1
	print()
