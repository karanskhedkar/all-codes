r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))
num=1
alpha=65
for i in range(r):
	for j in range(c):
		if(i%3==0):
			print("*",end=' ')
		elif(i%3==1):
			print(chr(alpha),end=' ')
			alpha=alpha+1
		elif(i%3==2):
			print(num,end=' ')
			num=num+1
		
	print()
