r=int(input("Enter number of rows : "))
c=int(input("Enter number of columns : "))
num=1
alpha=65

for i in range(r):
	for j in range(c):
		if(i%2==0):
			print(chr(alpha),end=' ')
			alpha=alpha+1
		else:
			print(num,end=' ')
			num=num+1
	print()