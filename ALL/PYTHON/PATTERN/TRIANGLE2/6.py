'''
10 9 8 7
6 5 4
3 2
1
'''
r=int(input("Enter number of rows : "))

num=(r*(r+1))/2

for i in range(r):
	for j in range(r-i):
		print(num,end=' ')
		num-=1
	print()