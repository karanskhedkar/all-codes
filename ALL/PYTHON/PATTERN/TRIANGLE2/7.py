'''
4 C 2 A
C 2 A
2 A
A
'''

r=int(input("Enter number of rows : "))

for i in range(r):
	num=(r-i)
	for j in range(r-i):
		if(num%2==0):
			print(num,end=' ')
		else:
			print(chr(num+64),end=" ")
		num-=1
	print()
