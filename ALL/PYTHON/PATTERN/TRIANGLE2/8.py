'''
E D C B A 
4 3 2 1
c b a 
B A
1
'''

r=int(input("Enter number of rows : "))

for i in range(r):
	num=(r-i)
	for j in range(r-i):
		if(i%3==0):
			print(chr(num+64),end=" ")
		elif(i%3==1):
			print(num,end=" ")
		else:
			print(chr(num+96),end=" ")
		num-=1
	print()
