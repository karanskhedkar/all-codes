'''
      2
    4 3
  6 6 4
8 9 8 5
'''

r = int(input("Enter number of rows : "))

for i in range(r):
	for j in range(r-i-1):
		print(" ",end=' ')

	num=2
	mul=i+1
	for k in range(i+1):
		print((num*mul),end=' ')
		num+=1
		mul-=1
	print()