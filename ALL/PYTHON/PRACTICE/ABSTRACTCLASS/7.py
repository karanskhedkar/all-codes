from abc import ABC,abstractmethod,abstractproperty
class Company(ABC):
	@abstractmethod
	def info(self):
		None
	@abstractproperty
	def disp(self):
		None
class Emp(Company):
	def __init__(self,name,sal):
		self.name=name
		self.sal=sal
	def info(self):
		print(self.name)
		print(self.sal)
	@property
	def disp(self):
		return self.name,self.sal
	@disp.setter
	def disp(self,newname):
		self.name=newname
obj=Emp('Karan',10)
obj.info()
print(obj.disp)
obj.disp='Sanyukta'
print(obj.disp)