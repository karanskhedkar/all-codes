#INITIALIZING A ARRAY

import array

num=array.array('i',[1,2,3,4,5,6,7,8,9])
alpha=array.array('u',['A', 'B','C','D'])
dec=array.array('f',[1.25,3.45,5.65,7.85,9.05])
print(num)
print(type(num))
print(len(num))
print()
print(alpha)
print(type(alpha))
print(len(alpha))
print()
print(dec)
print(type(dec))
print(len(dec))


