#SHALLOW COPY: IN SHALLOW COPY NO NEW OBJECT IS CREATED, NAME OF THE ARRAY JUST ACCESSE VALUE FROM MAIN ARRAY

import array

a1=array.array('i',[1,2,3,4,5])

a2=a1

print(a2)
print(a1)

print(id(a2))
print(id(a1))