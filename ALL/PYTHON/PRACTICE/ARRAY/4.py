#DEEP COPY: A NEW ARRAY WITH ITS OWN OBJECT IS CREATED, AND DATA FROM PREVIOUS ARRAY IS COPIED INTO NEW ONE

import array

a1=array.array('i',[1,2,3,4,5,6,7,8,9,10])

a2=array.array('i',(i for i in a1))
a3=a1
print(a1)
print(a2)

print(id(a1))
print(id(a2))
print(id(a3))

for i in range(len(a1)):
	print(a1[i])


