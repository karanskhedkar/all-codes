#USING DISPATCH DECORATOR
from multipledispatch import dispatch
class addition:

	@dispatch(int,int)
	def add(self,x,y):
		print(x+y)

	@dispatch(float,float)
	def add(self,x,y):
		print(x+y)

	@dispatch(int,int,int)
	def add(self,x,y,z):
		print(x+y+z)

	@dispatch(float,float,float)
	def add(self,x,y,z):
		print(x+y+z)
obj=addition()
obj.add(1,2)
obj.add(1.1,2.2)
obj.add(1,2,3)
obj.add(1.1,2.2,3.3)