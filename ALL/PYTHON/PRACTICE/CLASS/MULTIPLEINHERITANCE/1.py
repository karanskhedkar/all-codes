class A:
	def __init__(self):
		super().__init__()
		print('A Constructor')

class B(A):
	def __init__(self):
		super().__init__()
		print('B Constructor')

class C(A):
	def __init__(self):
		super().__init__()
		print('C Constructor')

class D(B):
	def __init__(self):
		super().__init__()
		print('D Constructor')

class E(B,C):
	def __init__(self):
		super().__init__()
		print('E Constructor')

class F(C):
	def __init__(self):
		super().__init__()
		print('F Constructor')

class G(D):
	def __init__(self):
		super().__init__()
		print('G Constructor')

class H(D,E):
	def __init__(self):
		super().__init__()
		print('H Constructor')

class I(E,F):
	def __init__(self):
		super().__init__()
		print('I Constructor')

class J(F):
	def __init__(self):
		super().__init__()
		print('J Constructor')

class K(G,H):
	def __init__(self):
		super().__init__()
		print('K Constructor')

class L(H,I):
	def __init__(self):
		super().__init__()
		print('L Constructor')

class M(I,J):
	def __init__(self):
		super().__init__()
		print('M Constructor')

class N(K,L):
	def __init__(self):
		super().__init__()
		print('N Constructor')

class O(L,M):
	def __init__(self):
		super().__init__()
		print('O Constructor')

class P(N,O):
	def __init__(self):
		super().__init__()
		print('P Constructor')
OBJ=P()