class A:
	def __init__(self):
		super().__init__()
		print('A Constructor')

class B(A):
	def __init__(self):
		super().__init__()
		print('B Constructor')

class C(A):
	def __init__(self):
		super().__init__()
		print('C Constructor')

class D(A):
	def __init__(self):
		super().__init__()
		print('D Constructor')

class E(B):
	def __init__(self):
		super().__init__()
		print('E Constructor')

class F(B):
	def __init__(self):
		super().__init__()
		print('F Constructor')

class G(C):
	def __init__(self):
		super().__init__()
		print('G Constructor')

class H(C):
	def __init__(self):
		super().__init__()
		print('H Constructor')

class I(D):
	def __init__(self):
		super().__init__()
		print('I Constructor')

class J(D):
	def __init__(self):
		super().__init__()
		print('J Constructor')

class K(E,F):
	def __init__(self):
		super().__init__()
		print('K Constructor')

class L(G,H):
	def __init__(self):
		super().__init__()
		print('L Constructor')

class M(I,J):
	def __init__(self):
		super().__init__()
		print('M Constructor')

class N(K,L,M):
	def __init__(self):
		super().__init__()
		print('N Constructor')
obj=N()