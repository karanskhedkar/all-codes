class A:
	def __init__(self):
		super().__init__()
		print('A Constructor')
class B(A):
	def __init__(self):
		super().__init__()
		print('B Constructor')
class C(A):
	def __init__(self):
		super().__init__()
		print('C Constructor')
class D(B):
	def __init__(self):
		super().__init__()
		print('D Constructor')
class E(C):
	def __init__(self):
		super().__init__()
		print('E Constructor')
class F(D):
	def __init__(self):
		super().__init__()
		print('F Constructor')
class G(E):
	def __init__(self):
		super().__init__()
		print('G Constructor')
class H(F,G):
	def __init__(self):
		super().__init__()
		print('H Constructor')
class I(H):
	def __init__(self):
		super().__init__()
		print('I Constructor')
class J(H):
	def __init__(self):
		super().__init__()
		print('J Constructor')
OBJ=I()