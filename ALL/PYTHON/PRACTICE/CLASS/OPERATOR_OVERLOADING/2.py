class A:
	def __init__(self,a):
		self.a=a
		print('A =',self.a)
	
	def __add__(self,obj):
		return self.a+obj.b

	def __sub__(self,obj):
		return self.a-obj.b

class B:
	def __init__(self,b):
		self.b=b
		print('B =',self.b)
	
	def __add__(self,obj1):
		return self.b+obj1.a

	def __sub__(self,obj1):
		return self.b-obj1.a
a=A(10)
b=B(20)
print('A+B =',a+b)
print('A-B =',a-b)
print('B+A =',b+a)
print('B-A =',b-a)