try:
	print(10/0)
	try:
		print('In inner try')
		print(0/0)
	#except:
	#	print('In inner except')
	finally:
		print('In inner finally')
	print(0/0)
except:
	print('In outer except')
finally:
	print('In outer finally')