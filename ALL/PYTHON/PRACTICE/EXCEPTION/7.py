class ABC:
	def __init__(self,a,b):
		self.a=a
		self.b=b
	def disp(self):
		print('A = ',self.a)
		print('B = ',self.b)
		try:
			print('B = ',self.c)
		except:
			print('C NOT FOUND')
print('Start code')
obj=ABC(2,3)
obj.disp()
try:
	obj.info()
except AttributeError:
	print('info() not found')
	print('Exp 2')
print('End code')