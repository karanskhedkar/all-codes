class InvalidUPIError(Exception):
	def __init__(self,ErrorCode):
		self.ErrorCode=ErrorCode

class TransactionData:
	def DataInfo(self):
		print('Access Granted to transaction History')

class UPI:
	def __init__(self,upi1,upi2):
		self.upi1=upi1
		self.upi2=upi2
	def check(self,Transaction):
		if(self.upi1==self.upi2):
			Transaction.DataInfo()
		else:
			raise InvalidUPIError('UPI Missmatched, access denied')

x='y'
while(x=='y' or x=='Y'):
	try:
		upi1=int(input('Enter UPI Password : '))
		try:
			upi2=int(input('Re-Enter UPI Password : '))
			x='n'
		except(ValueError,EOFError,KeyboardInterrupt):
			print('Invalid input')
			x=input('Do you want to retry : ')
	except(ValueError,EOFError,KeyboardInterrupt):
		print('Invalid Input')
		x=input('Do you want to retry : ')


Transaction=TransactionData()
User=UPI(upi1,upi2)
try:
	User.check(Transaction)
except InvalidUPIError as ErrorCode:
	print(ErrorCode)
