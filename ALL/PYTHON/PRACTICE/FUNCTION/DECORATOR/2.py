def rev(func):
	def inner(y):
		str1=func(y)
		str2=''
		for i in range(len(str1)):
			str2=str1[i]+str2
		return str2
	return inner
@rev
def prtstr(x):
	return x
n=input('Enter string : ')
print(prtstr(n))