def concat(func):
	def inner(*y):
		a=func(*y)
		print(a)
		b=''
		for i in range(len(a)):
			b=b+a[i]
		return b 
	return inner
@concat
def strs(*x):
	return x
n1=input('Enter first string : ')
n2=input('Enter second string : ')
print(strs(n1,n2))