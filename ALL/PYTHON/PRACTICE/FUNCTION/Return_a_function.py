def calc(x,y):
	def add():
		return x+y
	def mul():
		return x*y
	def diff():
		if(x>y):
			return x-y
		else:
			return y-x
	def div():
		if(x>y):
			return x/y
		else:
			return y/x
	return add,mul,diff,div
a=int(input('Enter first number : '))
b=int(input('Enter second number : '))
ans=calc(a,b)
for i in range(len(ans)):
	if(i==0):
		print('Sum :',ans[i]())
	elif(i==1):
		print('Product :',ans[i]())
	elif(i==2):
		print('Difference :',ans[i]())
	else:
		print('Quotient :',ans[i]())