import functools
'''
def mult(x,y):
	return x*y
'''
lst1=[1,2,3,4,5]
ans=functools.reduce(lambda x,y: x*y,lst1) # ans=functools.reduce(mult,lst1)
print(ans)