# IMPLEMENTATION OF INDEX() AND R RINDEX()

str1=input('Enter string : ')
char=input('Enter character to find : ')
index=-1
print()
#FIND
for i in range(len(str1)):
	if(str1[i]==char):
		index=i
		break
if(index==-1):
	print('RINDEX : ValueError')
else:
	print('INDEX : ',index)
print()
#RFIND
index=-1
for j in range(len(str1)):
	if(str1[j]==char):
		index=j
if(index==-1):
	print('RINDEX : ValueError')
else:
	print('RINDEX : ',index)
