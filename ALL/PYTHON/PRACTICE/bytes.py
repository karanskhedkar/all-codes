#1
a=[2,56,87,98,65]
byte = bytes(a)
print(a)
print(byte)
print(type(a))
print(type(byte))
print(id(a))
print(id(a[0]))

#2
a[0]=2000
print(a)
print(byte)
print(type(byte[0]))
print(a[0])
print(id(a))
print(id(a[0]))

#3
a[0]='a'
print(a)
print(byte)
print(type(byte[0]))
print(a[0])
print(id(a))
print(id(a[0]))
